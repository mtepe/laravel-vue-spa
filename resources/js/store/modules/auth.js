const state = {
    authenticated: false,
    user: null
}

const getters = {
    authenticated: state => state.authenticated,
    user: state => state.user,
}

const actions = {
    async login({dispatch}, credentials) {
        await axios.get('/sanctum/csrf-cookie');
        await axios.post('/login', credentials).then(res => console.log(res));

        return dispatch('authenticate');
    },

    async logout({dispatch}) {
        await axios.post('/logout');

        return dispatch('authenticate');
    },

    authenticate({commit}) {
        return axios.get('/api/user').then(res => {
            commit('SET_AUTHENTICATED', true);
            commit('SET_USER', res.data);
        }).catch(() => {
            commit('SET_AUTHENTICATED', false);
            commit('SET_USER', null);
        });
    }
}

const mutations = {
    SET_AUTHENTICATED: (state, value) => state.authenticated = value,
    SET_USER: (state, value) => state.user = value,
}

export default {state, getters, actions, mutations}
