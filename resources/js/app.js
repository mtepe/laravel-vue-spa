require('./bootstrap');

window.Vue = require('vue').default;

import router from './router';
import store from './store';
import App from "./layouts/App";

Vue.config.productionTip = false

store.dispatch('authenticate').then(() => {
    const app = new Vue({
        el: '#app',
        router,
        store,
        render: h => h(App),
    });
})


