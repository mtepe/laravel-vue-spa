import Vue from "vue";
import VueRouter from "vue-router";
import store from './store';

Vue.use(VueRouter);

import Login from "./views/Login";
import Dashboard from "./views/Dashboard";

const router = new VueRouter({
    mode: 'history',
    linkExactActiveClass: 'active',
    routes: [
        {
            path: '/',
            name: 'dashboard',
            component: Dashboard,
            meta: {
                requiresAuth: true,
            }
        },
        {
            path: '/login',
            name: 'login',
            component: Login,
            meta: {
                guest: true,
            }
        },
    ]
});

router.beforeEach((to, from, next) => {
    // you could define your own authentication logic with token
    let isAuthenticated = store.getters.authenticated

    // check route meta if it requires auth or not
    if(to.matched.some(record => record.meta.requiresAuth)) {
        if (!isAuthenticated) {
            next({
                path: '/login',
                params: { nextUrl: to.fullPath }
            })
        } else {
            next()
        }
    }else if(to.matched.some(record => record.meta.guest)) {
        if (isAuthenticated) {
            next({
                path: '/',
                params: { nextUrl: to.fullPath }
            })
        } else {
            next()
        }
    } else {
        next()
    }
})

export default router;
